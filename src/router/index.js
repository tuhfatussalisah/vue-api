import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/category',
    name: 'category.index',
    component: () => import("@/views/category/IndexView.vue")
  },
  {
    path: '/category/create',
    name: 'category.create',
    component: () => import("@/views/category/CreateView.vue")
  },
  {
    path: '/category/update/:id',
    name: 'category.update',
    component: () => import("@/views/category/UpdateView.vue")
  },
  {
    path: '/waste',
    name: 'waste.index',
    component: () => import("@/views/waste/IndexView.vue")
  },
  {
    path: '/waste/create',
    name: 'waste.create',
    component: () => import("@/views/waste/CreateView.vue")
  },
  {
    path: '/waste/update/:id',
    name: 'waste.update',
    component: () => import("@/views/waste/UpdateView.vue")
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
